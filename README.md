# local-gitea
Spin up a local containerized Gitea instance along with Drone and docker registry, for all your local ci-cd automation requirements.

## Instructions

Just follow these steps if you're on a linux machine:

1. Edit your `/etc/hosts` file to add the following lines:

   ```
   # You can choose any ip address, but I'd recommend a loopback
   # address, or a local bridge or virtual network interface,
   # to prevent other users from being able to connect to it,
   # unless that's what you want.
   127.0.3.4	gitea-web drone-server gitea-registry
   # Since all services are on different ports you can overload
   # the same ip address for all of them, but you can also use
   # different loopback addresses for them if you prefer.
   ```

   1. If you pick a different ip address, you'll need to modify the `docker-compose.yml` file to reflect that address in all the `ports:` sections of the file.

2. Start up the stack:

   ```shell
   $ docker-compose up -d ; docker-compose logs -f
   ```

3. Watch the logs go by; when it looks like all the services are browse the this location:
    `http://gitea-web:8080/` 

4. You will see the gitea startup page, you'll need to set some basic configuration items, the most important is a login for yourself.  When you save your info and re-login with the credentials you've set up you'll see the normal gitea dashboard with your account, repos, etc.

   1. Gitea documentation: https://docs.gitea.io/en-us/

5. Now you need to set up drone, go to this page: `http://drone-server:8000/` 
   This is the drone login screen, login with the same credentials you used for your gitea account, drone will be able to read your repositories and such; from here you will be able to view your jobs results, progress reports, etc.

   1. Drone documentation: https://docs.drone.io/

6. Your registry is located at: `gitea-registry:5000`

7. Enjoy!